import pytest

from tracker import Tracker


def test_tracking_calls():
    t = Tracker()

    for i in range(1, 11):
        t.call_me()
        assert t._times_called == i
