class Tracker:
    def __init__(self):
        self._times_called = 0

    def call_me(self):
        self._times_called += 1

    def times_called(self):
        return self._times_called
