\documentclass{beamer}
\usetheme{metropolis}

\usepackage{
  graphicx,
  minted
}

\title{Pytest Examples}
\author{Christopher Pitts}

\begin{document}
\maketitle

\section{Setup}
\begin{frame}{Setting the PYTHONPATH}
  Python imports libraries from a few set places:
  \begin{itemize}
  \item System install paths
  \item Prepackaged install paths
  \end{itemize}
\end{frame}

\begin{frame}{Setting the PYTHONPATH}
  You can also advise Python of other places it can import from, using
  the PYTHONPATH environment variable. Assuming you are in the root of
  the project, and you want to be able to import from a directory at
  the same level:

  \begin{itemize}
  \item *NIX: export PYTHONPATH=.
  \item Windows: set PYTHONPATH \textquotedbl .\textquotedbl
  \end{itemize}
\end{frame}

\begin{frame}{Running pytest}
  The \textbf{pytest} command is the most flexible way to run your
  tests. You invoke it on the command line with ``pytest'' or
  ``py.test'', depending on your platform and version (usually either
  command will work).
\end{frame}

\section{Basics}
\begin{frame}{Assertions}
  An assertion in code is just that: you \emph{assert} what the state
  of the program (or some part of it) should be after executing one or
  more statements. If the assertion is true, nothing happens. If it's
  false, the test fails.
\end{frame}

\begin{frame}{Function Assertions}
  Given a function like this:
  \inputminted{python}{assets/code/add.py}
\end{frame}

\begin{frame}{Function Assertions}
  You could write an assertion to test it like this:
  \inputminted{python}{assets/code/test_add.py}
\end{frame}

\begin{frame}{Function Assertions}
  This assertion would fail:
  \inputminted{python}{assets/code/test_add_fail.py}
\end{frame}

\begin{frame}{Class Assertions}
  When testing a class, you generally make assertions about the
  internal state of the object.
\end{frame}

\begin{frame}{Class Assertions}
  Given this class:
  \inputminted{python}{assets/code/tracker.py}
\end{frame}

\begin{frame}{Class Assertions}
  You could write a test like this:
  \inputminted{python}{assets/code/test_tracker.py}
\end{frame}

\section{Parametrizing Tests}
\begin{frame}{The Problem}
  A test might have different configurations you want to run. Consider
  the addition example from earlier, you might want to test the
  addition with:

  \begin{enumerate}
  \item Positive numbers
  \item Negative numbers
  \item Positive and negative numbers
  \item Zeroes
  \end{enumerate}

  Writing tests for all of these would be time-consuming, involve a
  lot of copy-pasting, and would be impossible to maintain.
\end{frame}

\begin{frame}{The Solution}
  \emph{Parametrizing} is the technique of defining a test function
  that takes parameters, which allows for tests to be repeated with
  different values. You could write a test function like this:
  \inputminted[fontsize=\footnotesize]{python}{assets/code/test_add_parametrized.py}
\end{frame}

\begin{frame}{The Solution}
  Notice that the first argument to the parametrize decorator is a
  string listing the parameters to the function. The second argument
  is a list of argument tuples to pass to the function.
\end{frame}

\section{Mocking}
\begin{frame}{Why Mock?}
  Consider the following example: you have a database connection as
  part of your program, and would like to test the following method:
  \inputminted[fontsize=\footnotesize]{python}{assets/code/db.py}
\end{frame}

\begin{frame}{Why Mock?}
  Writing tests for this function could look something like this:
  \inputminted[fontsize=\footnotesize]{python}{assets/code/test_db.py}
\end{frame}

\begin{frame}{Why Mock?}
  But, there's a problem with this: these tests require a running
  database with data. Some problems with this:

  \begin{itemize}
  \item You now have to have a live database for each developer to run
    tests on.
  \item You have to spin up/tear down the database, and maintain test
    data in it.
  \item Databases aren't free.
  \end{itemize}
\end{frame}

\begin{frame}{Why Mock?}
  \emph{Mocking}, also known as \emph{monkeypatching}, is the
  replacement of a system component with an object that has the same
  interface but just returns predetermined data. This allows you to
  simulate parts of a system that you might not be able to have
  on-demand for tests, such as databases, web servers, or sensors.
\end{frame}

\begin{frame}{Monkeypatching}
  The pytest library provides a monkeypatching module that you can use
  by including \textbf{monkeypatch} as a parameter in a test
  function. You can replace a function by name like this:
  \inputminted[fontsize=\footnotesize]{python}{assets/code/monkeypatch.py}
\end{frame}

\begin{frame}{Monkeypatching}
  Rewriting the \textbf{get\_user} tests might look like this:
  \inputminted[fontsize=\footnotesize]{python}{assets/code/test_db_mocking.py}
\end{frame}

\begin{frame}{Other Mocking Examples}
  You can also mock user input:
  \inputminted{python}{assets/code/test_mock_input.py}
\end{frame}

\section{Plugins}
\begin{frame}{Extra Goodies}
  pytest offers a lot of functionality out of the box, but there is
  also a large set of plugins that can extend the functionality. Some
  examples of plugins:

  \begin{itemize}
  \item Test coverage
  \item Docstring testing (for the truly dedicated)
  \item Ordering tests
  \end{itemize}
\end{frame}

\begin{frame}{Test Coverage With pytest-cov}
  Using the \textbf{pytest-cov} plugin, you can output the test
  coverage for a particular module like this:

  \textbf{pytest -{}-cov=my\_module -{}-cov-report=term-missing}

  This will output the test coverage to the command line, as well as
  tell you which lines aren't currently covered.
\end{frame}
\end{document}